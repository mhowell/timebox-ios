//
//  DetailViewController.h
//  TimeBox
//
//  Created by Matthew Howell on 17/02/13.
//  Copyright (c) 2013 Matthew Howell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
